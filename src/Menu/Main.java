package Menu;

import java.util.Scanner;

public class Main {

	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		Double medida;
				
		System.out.println("****** Selecione a escala de temperatura ******** ");
		System.out.println("1 - Kelvin");
		System.out.println("2 - Celsius");
		System.out.println("3 - Fahrenheit");
		
		medida= input.nextDouble();
		
		System.out.printf("O valor de temperatura informado foi: %f\n",medida.floatValue());
		input.close();
	}
}