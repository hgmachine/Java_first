package Termometro;

public class Celsius extends Temperatura {

	public String converter(String medida)
	{		
		super.setTemperatura(medida);
		Double KelvinValue = super.getTemperatura() + 273;
		Double FahrenheitValue = super.getTemperatura() * 1.8 - 32;
		return String.format("Temperatura em Graus Celsius = %f, Temperatura em Graus " +
				"Fahrenheit = %f, Temperatura em Graus Kelvin = %f",
				super.getTemperatura().floatValue(),FahrenheitValue.floatValue(),KelvinValue.floatValue());
	}
}
