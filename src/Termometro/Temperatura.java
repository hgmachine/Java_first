package Termometro;

public class Temperatura {
	
	private Double value;
	
	public String converter(String medida)
	{
		setTemperatura(medida);		
		Double CelsiusValue = value - 273;
		Double FahrenheitValue = value * 1.8 - 459.67;
		return String.format("Temperatura em Graus Celsius = %f, Temperatura em Graus " +
				"Fahrenheit = %f, Temperatura em Graus Kelvin = %f",
				CelsiusValue.floatValue(),FahrenheitValue.floatValue(),value.floatValue());
	}
	
	public void setTemperatura(String medida)
	{
		this.value= Double.valueOf(medida);
	}
	
	public Double getTemperatura()
	{
		return value;
	}
}